Otrhodox Calendar project initially forked from https://launchpad.net/orthcal for GTK2 Python2/Perl Environment, revised to use Python3, hoping that it will keep on going.

Applet Tray Icon

![Screenshot](https://i.imgur.com/FygTVlJ.jpg) 

Applet Tray Menu

![Screenshot](https://imgur.com/PzifpgU.jpg)

Applet Languages

![Screenshot](https://imgur.com/TS5x8VK.png)

Applet window Darkthemed for GTK dark themes

![Screenshot](https://imgur.com/apr0rhz.png)

Applet Main Window Background Lightthemed in order to be readable in light GTK themes

![Screenshot](https://imgur.com/9Nnoz2T.jpg)

**- Both versions will be provided on master tags, in separate files as from 30th of December 2019**

**- Packages for Arch Linux, already exist on AUR**

Prerequisites:

-perl

-Perl modules:
        
        Date::Calc (packaged as 'libdate-calc-perl' in Ubuntu, 'perl-date-calc' in ARCH)
        
        DBI (packaged as 'libdbi-perl' and 'libdbd-sqlite3-perl' in Ubuntu, 'perl-dbi' and 'perl-dbd-sqlite' in ARCH)
        
        Gtk2 (packaged as 'libgtk2-perl' in Ubuntu. 'perl-gtk2-trayicon' in ARCH, yes no perl gtk3 modules yet, hope that they will be provided)

-python3

-PyGTK  (packaged as 'python-gtk2' in Ubuntu, 'pygtk' in ARCH)

-Python appindicator (packaged as 'python-appindicator' in Ubuntu,'python2-libappindicator' in ARCH)
