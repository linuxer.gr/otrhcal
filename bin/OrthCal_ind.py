#!/usr/bin/python3
# -*- coding: utf-8 -*-

#
# Copyright 2012 Dimitrios - Georgios Kontopoulos
#    <dgkontopoulos[at]member[dot]fsf[dot]org>
# 
# Relicensed under GPL3 by Project's Maintainer Paschalis Sposito 2018 on GitLab https://gitlab.com/psposito/otrhcal
# Revised to python3 from the previous maintained python2 Orthcal at 29/12/2019
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License, as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details at
#  <http://www.gnu.org/licenses>.
#
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('AppIndicator3', '0.1')
try:
     from gi.repository import AppIndicator3 as appindicator
     have_indicator = True
except:
     have_indicator = False
from gi.repository import Gtk as gtk
import getpass
from gi.repository import GObject as gobject
import locale
import os.path
import subprocess
import sys

from locale import gettext as _
locale.bindtextdomain('OrthCal_ind', '/opt/orthcal/translations/locale')
locale.textdomain('OrthCal_ind')
 
class StatusIcon:
    def __init__(self):
        self.statusicon = gtk.StatusIcon()
        self.statusicon.set_from_file("/opt/orthcal/content/images/orthcal_ind.png") 
        self.statusicon.connect("popup-menu", self.right_click_event)
        self.statusicon.set_title("OrthCal")       
		
    def right_click_event(self, icon, button, time):
        menu = gtk.Menu()

        feastsJulian = _("Today's Feasts (Julian/Old Calendar)")
        menu_feastsJulian = gtk.MenuItem(feastsJulian)
        menu.append(menu_feastsJulian)
        menu_feastsJulian.connect('activate', julian_calendar, feastsJulian)
        menu_feastsJulian.show()

        feastsRJulian = _("Today's Feasts (Revised Julian/New Calendar)")
        menu_feastsRJulian = gtk.MenuItem(feastsRJulian)
        menu.append(menu_feastsRJulian)
        menu_feastsRJulian.connect('activate', rjulian_calendar, feastsRJulian)
        menu_feastsRJulian.show()

        separator1 = gtk.SeparatorMenuItem()
        menu.append(separator1)
        separator1.show()
        
        options = _('Options')
        menu_options = gtk.MenuItem(options)
        menu.append(menu_options)
        menu_options.connect('activate', OrthCal_options, options)
        menu_options.show()

        about = _('About OrthCal')
        menu_about = gtk.MenuItem(about)
        menu.append(menu_about)
        menu_about.connect('activate', about_OrthCal, about)
        menu_about.show()

        separator2 = gtk.SeparatorMenuItem()
        menu.append(separator2)
        separator2.show()

        quit = _('Quit')
        menu_quit = gtk.MenuItem(quit)
        menu.append(menu_quit)
        menu_quit.connect('activate', destroy, quit)
        menu_quit.show()
        
        menu.popup(None, None, gtk.status_icon_position_menu, button, time, self.statusicon)
       
def about_OrthCal(w, about):
    subprocess.call('/opt/orthcal/bin/OrthCal.pl about&', shell=True)

def destroy(appindicator, data=None):
    gtk.main_quit()

def julian_calendar(w, feastsJulian):
    subprocess.call('/opt/orthcal/bin/OrthCal.pl Julian&', shell=True)

def OrthCal_options(w, options):
    subprocess.call('/opt/orthcal/bin/OrthCal.pl options&', shell=True)

def rjulian_calendar(w, feastsRJulian):
    subprocess.call('/opt/orthcal/bin/OrthCal.pl RJulian&', shell=True)


if __name__ == '__main__':
    if have_indicator == True:
      
        ind = appindicator.Indicator.new('OrthCal', '/opt/orthcal/content/images/orthcal_ind.png', appindicator.IndicatorCategory.APPLICATION_STATUS)
        ind.set_status(appindicator.IndicatorStatus.ACTIVE)

        menu = gtk.Menu()

        feastsJulian = _("Today's Feasts (Julian/Old Calendar)")
        menu_feastsJulian = gtk.MenuItem.new_with_label(feastsJulian)
        menu.append(menu_feastsJulian)
        menu_feastsJulian.connect('activate', julian_calendar, feastsJulian)
        menu_feastsJulian.show()

        feastsRJulian = _("Today's Feasts (Revised Julian/New Calendar)")
        menu_feastsRJulian = gtk.MenuItem.new_with_label(feastsRJulian)
        menu.append(menu_feastsRJulian)
        menu_feastsRJulian.connect('activate', rjulian_calendar, feastsRJulian)
        menu_feastsRJulian.show()

        separator1 = gtk.SeparatorMenuItem()
        menu.append(separator1)
        separator1.show()
        
        options = _('Options')
        menu_options = gtk.MenuItem.new_with_label(options)
        menu.append(menu_options)
        menu_options.connect('activate', OrthCal_options, options)
        menu_options.show()

        about = _('About OrthCal')
        menu_about = gtk.MenuItem.new_with_label(about)
        menu.append(menu_about)
        menu_about.connect('activate', about_OrthCal, about)
        menu_about.show()

        separator2 = gtk.SeparatorMenuItem()
        menu.append(separator2)
        separator2.show()

        quit = _('Quit')
        menu_quit = gtk.MenuItem.new_with_label(quit)
        menu.append(menu_quit)
        menu_quit.connect('activate', destroy, quit)
        menu_quit.show()

        ind.set_menu(menu)

        gtk.main()
    else:
	    StatusIcon()
	    gtk.main()
