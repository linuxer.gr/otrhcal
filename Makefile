INSTALLDIR=/opt/orthcal

Scripts: bin/OrthCal.pl bin/OrthCal_ind.py
	chmod 755 bin/OrthCal.pl
	chmod 755 bin/OrthCal_ind.py

install:
	sudo mkdir -p $(INSTALLDIR)/bin/
	sudo mkdir -p $/usr/share/applications/
	sudo cp bin/OrthCal.pl $(INSTALLDIR)/bin/
	sudo cp bin/OrthCal_ind.py $(INSTALLDIR)/bin/
	sudo cp -R content/ $(INSTALLDIR)
	sudo cp -R translations/ $(INSTALLDIR)
	sudo cp -f extras-orthcal.desktop /usr/share/applications/

uninstall:
	sudo rm -rf $(INSTALLDIR)
	sudo rm -rf /usr/share/applications/extras-orthcal.desktop
