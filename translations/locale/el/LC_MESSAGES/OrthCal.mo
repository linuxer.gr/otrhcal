��    C      4  Y   L      �     �     �     �     �  :   �       5  6     l  9   �  �  �  �  W
  �  �  �  �       
   �     �     �     �  9   �  	             +     ?  %   V     |     �     �     �     �     �     �     �     �     �       +        I     _     x     �     �     �     �     �     �     �           :     O     m  $   �  "   �  "   �     �                9     Q     l     �     �     �     �     �     �     �  x  �  #   O  #   s     �  
   �  ;   �  +   �  �    -   �  p   �  h  >    �    �!  l  �$     )'     A'     V'  <   p'  &   �'  o   �'     D(  #   ](  (   �(  $   �(  C   �(  *   )  9   >)     x)     �)     �)     �)     �)     *     *      +*  
   L*  ^   W*     �*  6   �*     
+      *+  
   K+  3   V+  ?   �+  ,   �+  7   �+  1   /,  Y   a,  -   �,  (   �,  *   -  I   =-  I   �-  I   �-  +   .  "   G.  :   j.  (   �.  "   �.  .   �.      /  /   7/  $   g/     �/     �/     �/     �/     2      6   ?   -          &   %               !   	   9            A   *   
         @      4   :       (                  5          /   ,      +         $      7                >      )                          "   <                   =              .   #   C          8              '   1          ;       3   B   0        days after Pascha  days before Pascha  is:  was: <b>Double click</b> to change from one source to another.
 <b>Lazarus</b> Saturday <b>OrthCal, the Eastern Orthodox Calendar</b>, v. 1.4
		        <a href="http://launchpad.net/orthcal">Homepage</a> - - <a href="https://www.transifex.com/projects/p/OrthCal">Translations</a>
         (C) 2012 <a href="mailto:dgkontopoulos@member.fsf.org?Subject=OrthCal">Dimitrios - Georgios Kontopoulos</a>
 <b>Zacchaeus</b> Sunday <i>Please restart OrthCal for changes to take effect.</i> <span size="small"><b><u>Content sources:</u></b>
	➜The daily feasts have been extracted from <a href="http://orthodoxwiki.org/">OrthodoxWiki</a> (CC BY-
SA 2.5), <a href="http://www.saint.gr">saint.gr</a> (<i>"freely ye have received, freely give"</i>) and <a href="http://drevo-info.ru/">Древо</a> (Public
Domain).
	➜The background picture of a cross has been borrowed under the
CC BY 2.0 license from <a href="http://www.flickr.com/photos/brantley/5173197588/">Peter Brantley</a>.
	➜The application's icon has been borrowed under the CC BY-SA
2.0 license from <a href="http://www.flickr.com/photos/59838910@N06/7715907356/">israeltourism</a>.</span>
 <span size="small"><b><u>License:</u></b>
<i>This program is free software; you can redistribute it and/or modify it
under the terms of the <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License, as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version</a>.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</i>

Images and content mentioned at the Content Sources section are
offered under their original respective licenses.</span>
 <span size="small"><b><u>Thanks to:</u></b>
	➜<b>Adolfo Jayme Barrientos</b> for translating OrthCal to Spanish.
	➜<b>Ciprian M Cornea</b> and <b>Ovidiu-Constantin Teaca</b> for translating
OrthCal to Romanian.
	➜<b>Rodion Renzhin</b> for editing the Russian feasts content and
translating OrthCal to Russian.
	➜the members of the <b><a href='http://ubuntu.gr'>Ubuntu-gr community</a></b> for bugs detection,
feature suggestions and general support.</span>
 <span size="small">This application was written for the <a href="http://developer.ubuntu.com/showdown/">Ubuntu App Showdown contest</a> of
July 2012, mainly as practice.

The indicator's code is written in <a href="http://www.python.org/">Python</a>/<a href="http://www.pygtk.org/">PyGTK</a>, whereas the main part
of the calendar is written in <a href="http://www.perl.org/">Perl</a>/<a href="http://search.cpan.org/~xaoc/Gtk2-1.244/lib/Gtk2.pm">GTK2</a>.</span>
 About OrthCal About Perl About gtk2-perl All Saints of North America Ascension Thursday Cheesefare (Forgiveness) Sunday / Expulsion from Paradise Completed Content sources Daily Feasts source Day of the Holy Spirit Fathers of the 1st Ecumenical Council Feast search -- OrthCal Great Lent begins Holy Friday Holy Monday Holy Saturday Holy Thursday Holy Tuesday Holy Wednesday Language Leavetaking of Pascha License Meat Fare Sunday (Sunday of Last Judgement) Midfeast of Pentecost No feast could be found. Options for OrthCal Palm Sunday Pascha Pascha calculator -- OrthCal Pascha date calculator Pentecost Sunday Publican & Pharisee Sunday Search for feasts by name Start OrthCal on system startup. Sunday of All Saints Sunday of Myrrh-bearing Women Sunday of Orthodoxy Sunday of St. <b>Gregory</b> Palamas Sunday of St. <b>John</b> Climacus Sunday of St. <b>Mary</b> of Egypt Sunday of St. <b>Thomas</b> Sunday of the Blind Man Sunday of the Holy Cross Sunday of the Paralytic Sunday of the Prodigal Son Sunday of the Samaritan Woman Thanks The date of Pascha in  Translate OrthCal... _Edit _File _Help _Tools Project-Id-Version: OrthCal
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-10-21 18:26+0300
PO-Revision-Date: 2012-10-21 15:35+0000
Last-Translator: dgkontopoulos <dgkontopoulos@member.fsf.org>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 μέρες μετά το Πάσχα μέρες πριν το Πάσχα  είναι:  ήταν: <b>Διπλό κλικ</b> για αλλαγή πηγής.
 Κυριακή του <b>Λαζάρου</b> <b>OrthCal, το Ανατολικό Ορθόδοξο Ημερολόγιο</b>, v. 1.4
		        <a href="http://launchpad.net/orthcal">Ιστοσελίδα</a> - - <a href="https://www.transifex.com/projects/p/OrthCal">Μεταφράσεις</a>
         (C) 2012 <a href="mailto:dgkontopoulos@member.fsf.org?Subject=OrthCal">Δημήτριος - Γεώργιος Κοντόπουλος</a>
 Κυριακή του <b>Ζακχαίου</b> <i>Επανεκκινήστε το OrthCal για να πραγματοποιηθούν οι αλλαγές.</i> <span size="small"><b><u>Πηγές περιεχομένου:</u></b>
	➜Οι ημερήσιες εορτές έχουν εξαχθεί από το <a href="http://orthodoxwiki.org/">OrthodoxWiki</a> (CC BY-SA 2.5)
το <a href="http://www.saint.gr">saint.gr</a> (<i>"δωρεὰν ἐλάβετε, δωρεὰν δότε"</i>) και το <a href="http://drevo-info.ru/">Древо</a> (Κοινό Κτήμα).
	➜Η εικόνα παρασκηνίου με το σταυρό είναι δανειζόμενη υπό την άδεια
CC BY 2.0 από τον <a href="http://www.flickr.com/photos/brantley/5173197588/">Peter Brantley</a>.
	➜Το εικονίδιο της εφαρμογής είναι δανειζόμενο υπό την άδεια CC BY-SA
2.0 από την <a href="http://www.flickr.com/photos/59838910@N06/7715907356/">israeltourism</a>.</span>
 <span size="small"><b><u>Άδεια:</u></b>
<i>Αυτό το πρόγραμμα είναι ελεύθερο λογισμικό: επιτρέπεται η αναδιανομή ή/
και τροποποίησή του υπό τους όρους της <a href="http://www.gnu.org/licenses/gpl.html">Γενικής Άδειας Δημόσιας Χρήσης
GNU, όπως αυτή έχει δημοσιευτεί από το Ίδρυμα Ελεύθερου Λογισμικού (Free
Software Foundation) -είτε της έκδοσης 3 της ‘Αδειας, είτε (κατ' επιλογήν)
οποιασδήποτε μεταγενέστερης έκδοσης</a>.

Το πρόγραμμα αυτό διανέμεται με την ελπίδα ότι θα αποδειχθεί χρήσιμο,
παρόλα αυτά ΧΩΡΙΣ ΚΑΜΙΑ ΕΓΓΥΗΣΗ –χωρίς ούτε και την σιωπηρή εγγύηση
ΕΜΠΟΡΕΥΣΙΜΟΤΗΤΑΣ ή ΚΑΤΑΛΛΗΛΟΤΗΤΑΣ ΓΙΑ ΣΥΓΚΕΚΡΙΜΕΝΗ ΧΡΗΣΗ.</i>

Οι εικόνες και το περιεχόμενο που αναφέρονται στην ενότητα Πηγές
Περιεχομένου προσφέρονται υπό τις αντίστοιχες αρχικές τους άδειες.</span>
 <span size="small"><b><u>Ευχαριστίες προς:</u></b>
	➜τον <b>Adolfo Jayme Barrientos</b> για τη μετάφραση του OrthCal στα
Ισπανικά.
	➜τους <b>Ciprian M Cornea</b> και <b>Ovidiu-Constantin Teaca</b> για τη
μετάφραση του OrthCal στα Ρουμανικά.
	➜τον <b>Rodion Renzhin</b> για την επεξεργασία του Ρωσικού περιεχομένου
εορτών και για τη μετάφραση του OrthCal στα Ρωσικά.
	➜τα μέλη της <b><a href='http://ubuntu.gr'>κοινότητας Ubuntu-gr</a></b> για τον εντοπισμό bugs, τις
προτάσεις για νέα features και τη γενικότερη υποστήριξη.</span>
 <span size="small">Η εφαρμογή αυτή γράφτηκε για το <a href="http://developer.ubuntu.com/showdown/">διαγωνισμό Ubuntu App Showdown</a> του
Ιουλίου του 2012, κυρίως για εξάσκηση.

Ο κώδικας του δείκτη είναι γραμμένος σε <a href="http://www.python.org/">Python</a>/<a href="http://www.pygtk.org/">PyGTK</a>, ενώ το κύριο μέρος
του ημερολογίου είναι γραμμένο σε <a href="http://www.perl.org/">Perl</a>/<a href="http://search.cpan.org/~xaoc/Gtk2-1.244/lib/Gtk2.pm">GTK2</a>.</span>
 Περί του OrthCal Περί της Perl Περί του gtk2-perl Πάντες οι Άγιοι Βορείου Αμερικής Πέμπτη της Αναλήψεως Κυριακή της Τυροφάγου (Συγχώρεσης) / Εξορία από τον Παράδεισο Ολοκληρωμένη Πηγές περιεχομένου Πηγή Ημερησίων Εορτών Του Αγίου Πνεύματος Πατέρες της 1ης Οικουμενικής Συνόδου Αναζήτηση εορτών -- OrthCal Αρχή της Μεγάλης Τεσσαρακοστής Μεγάλη Παρασκευή Μεγάλη Δευτέρα Μεγάλο Σάββατο Μεγάλη Πέμπτη Μεγάλη Τρίτη Μεγάλη Τετάρτη Γλώσσα Απόδοση του Πάσχα Άδεια Κυριακή της Απόκρεω (Κυριακή της Τελευταίας Κρίσης) Μεσοπεντηκοστή Δεν εντοπίστηκε κάποια εορτή. Επιλογές του OrthCal Κυριακή των Βαΐων Πάσχα Υπολογισμός του Πάσχα -- OrthCal Υπολογισμός ημερομηνίας του Πάσχα Κυριακή της Πεντηκοστής Κυριακή Τελώνου και Φαρισαίου Αναζήτηση εορτών για όνομα Εκκίνηση του OrthCal κατά την έναρξη του υπολογιστή. Κυριακή των Αγίων Πάντων Κυριακή των Μυροφόρων Κυριακή της Ορθοδοξίας Κυριακή του Αγ. <b>Γρηγορίου</b> του Παλαμά Κυριακή του Αγ. <b>Ιωάννου</b> της Κλίμακας Κυριακή της Αγ. <b>Μαρίας</b> της Αιγυπτίας Κυριακή του Αγ. <b>Θωμά</b> Κυριακή του Τυφλού Κυριακή της Σταυροπροσκυνήσεως Κυριακή του Παραλύτου Κυριακή του Ασώτου Κυριακή της Σαμαρείτιδος Ευχαριστίες Η ημερομηνία του Πάσχα το  Μεταφράστε το OrthCal... _Επεξεργασία _Αρχείο _Βοήθεια Εργα_λεία 