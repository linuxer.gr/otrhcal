��    C      4  Y   L      �     �     �     �     �  :   �       5  6     l  9   �  �  �  �  W
  �  �  �  �       
   �     �     �     �  9   �  	             +     ?  %   V     |     �     �     �     �     �     �     �     �     �       +        I     _     x     �     �     �     �     �     �     �           :     O     m  $   �  "   �  "   �     �                9     Q     l     �     �     �     �     �     �     �  �  �     �     �     �     �  K     &   T  \  {  #   �  u   �    r  �  �  w  8!  %  �#     �%     �%     �%  �   �%  %   y&  �   �&     ('  /   9'  '   i'      �'  `   �'  1   (  (   E(     n(  %   �(     �(     �(     �(     )     &)  ,   /)     \)  A   m)  3   �)  )   �)     *  %   (*  
   N*     Y*  (   u*  ?   �*  .   �*  5   +  >   C+  H   �+  D   �+  X   ,     i,  a   �,  ]   K-  �   �-  2   ,.  M   _.  >   �.  '   �.  :   /     O/  '   j/     �/     �/  	   �/     �/     �/     2      6   ?   -          &   %               !   	   9            A   *   
         @      4   :       (                  5          /   ,      +         $      7                >      )                          "   <                   =              .   #   C          8              '   1          ;       3   B   0        days after Pascha  days before Pascha  is:  was: <b>Double click</b> to change from one source to another.
 <b>Lazarus</b> Saturday <b>OrthCal, the Eastern Orthodox Calendar</b>, v. 1.4
		        <a href="http://launchpad.net/orthcal">Homepage</a> - - <a href="https://www.transifex.com/projects/p/OrthCal">Translations</a>
         (C) 2012 <a href="mailto:dgkontopoulos@member.fsf.org?Subject=OrthCal">Dimitrios - Georgios Kontopoulos</a>
 <b>Zacchaeus</b> Sunday <i>Please restart OrthCal for changes to take effect.</i> <span size="small"><b><u>Content sources:</u></b>
	➜The daily feasts have been extracted from <a href="http://orthodoxwiki.org/">OrthodoxWiki</a> (CC BY-
SA 2.5), <a href="http://www.saint.gr">saint.gr</a> (<i>"freely ye have received, freely give"</i>) and <a href="http://drevo-info.ru/">Древо</a> (Public
Domain).
	➜The background picture of a cross has been borrowed under the
CC BY 2.0 license from <a href="http://www.flickr.com/photos/brantley/5173197588/">Peter Brantley</a>.
	➜The application's icon has been borrowed under the CC BY-SA
2.0 license from <a href="http://www.flickr.com/photos/59838910@N06/7715907356/">israeltourism</a>.</span>
 <span size="small"><b><u>License:</u></b>
<i>This program is free software; you can redistribute it and/or modify it
under the terms of the <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License, as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version</a>.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</i>

Images and content mentioned at the Content Sources section are
offered under their original respective licenses.</span>
 <span size="small"><b><u>Thanks to:</u></b>
	➜<b>Adolfo Jayme Barrientos</b> for translating OrthCal to Spanish.
	➜<b>Ciprian M Cornea</b> and <b>Ovidiu-Constantin Teaca</b> for translating
OrthCal to Romanian.
	➜<b>Rodion Renzhin</b> for editing the Russian feasts content and
translating OrthCal to Russian.
	➜the members of the <b><a href='http://ubuntu.gr'>Ubuntu-gr community</a></b> for bugs detection,
feature suggestions and general support.</span>
 <span size="small">This application was written for the <a href="http://developer.ubuntu.com/showdown/">Ubuntu App Showdown contest</a> of
July 2012, mainly as practice.

The indicator's code is written in <a href="http://www.python.org/">Python</a>/<a href="http://www.pygtk.org/">PyGTK</a>, whereas the main part
of the calendar is written in <a href="http://www.perl.org/">Perl</a>/<a href="http://search.cpan.org/~xaoc/Gtk2-1.244/lib/Gtk2.pm">GTK2</a>.</span>
 About OrthCal About Perl About gtk2-perl All Saints of North America Ascension Thursday Cheesefare (Forgiveness) Sunday / Expulsion from Paradise Completed Content sources Daily Feasts source Day of the Holy Spirit Fathers of the 1st Ecumenical Council Feast search -- OrthCal Great Lent begins Holy Friday Holy Monday Holy Saturday Holy Thursday Holy Tuesday Holy Wednesday Language Leavetaking of Pascha License Meat Fare Sunday (Sunday of Last Judgement) Midfeast of Pentecost No feast could be found. Options for OrthCal Palm Sunday Pascha Pascha calculator -- OrthCal Pascha date calculator Pentecost Sunday Publican & Pharisee Sunday Search for feasts by name Start OrthCal on system startup. Sunday of All Saints Sunday of Myrrh-bearing Women Sunday of Orthodoxy Sunday of St. <b>Gregory</b> Palamas Sunday of St. <b>John</b> Climacus Sunday of St. <b>Mary</b> of Egypt Sunday of St. <b>Thomas</b> Sunday of the Blind Man Sunday of the Holy Cross Sunday of the Paralytic Sunday of the Prodigal Son Sunday of the Samaritan Woman Thanks The date of Pascha in  Translate OrthCal... _Edit _File _Help _Tools Project-Id-Version: OrthCal
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-10-21 18:26+0300
PO-Revision-Date: 2012-10-22 08:20+0000
Last-Translator: dgkontopoulos <dgkontopoulos@member.fsf.org>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru_RU
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
  дней после Пасхи  дней до Пасхи  совершается:  совершалось: <b>Дважды щёлкните</b> для смены источника.
 <b>Лазарева</b> Суббота <b>OrthCal, Православный календарь</b>, v. 1.4
           <a href="http://launchpad.net/orthcal">Домашняя страница</a> - - <a href="https://www.transifex.com/projects/p/OrthCal">Переводы</a>
     © 2012 <a href="mailto:dgkontopoulos@member.fsf.org?Subject=OrthCal">Dimitrios - Georgios Kontopoulos</a>
 Неделя о <b>Закхее</b> Пожалуйста, перезапустите OrthCal, чтобы изменения вступили в силу. <span size="small"><b><u>Источники:</u></b>
	➜Ежедневные праздники извлечены из <a href="http://orthodoxwiki.org/">OrthodoxWiki</a> (CC BY-
SA 2.5), <a href="http://www.saint.gr">saint.gr</a> (<i>"даром получили, даром давайте"</i>) и <a href="http://drevo-info.ru/">Древа</a>
(общественное достояние).
	➜Фоновое изображение креста заимствовано под CC BY
2.0 лицензией у <a href="http://www.flickr.com/photos/brantley/5173197588/">Peter Brantley</a>.
	➜Значок приложения заимствован под CC BY-SA 2.0
лицензией у <a href="http://www.flickr.com/photos/59838910@N06/7715907356/">israeltourism</a>.</span>
 <span size="small"><b><u>Лицензия:</u></b>
<i>Эта свободное программное обеспечение; вы можете распространять и/или
изменять его на условиях лицензии <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License</a>, опубликованной
Фондом свободного программного обеспечения; либо версии 3 этой
лицензии, либо (по вашему выбору) любой более поздней версии.

Эта программа распространяется в надежде, что может быть полезна, но
БЕЗ КАКОГО-ЛИБО ВИДА ГАРАНТИЙ; не ограничиваясь подразумеваемыми
гарантиями КОММЕРЧЕСКОЙ ЦЕННОСТИ или ПРИГОДНОСТИ ДЛЯ КОНКРЕТНОЙ
ЦЕЛИ.</i>

Изображения и содержимое, упомянутые в Источниках содержимого,
использованы в соответствии с их оригинальными лицензиями.</span>
 <span size="small"><b><u>Благодарности:</u></b>
	➜<b>Adolfo Jayme Barrientos</b> за перевод OrthCal на испанский.
	➜<b>Ciprian M Cornea</b> и <b>Ovidiu-Constantin Teaca</b> за перевод
OrthCal на румынский.
	➜<b>Родиону Ренжину</b> за редактирование русскоязычных
святцев и перевод OrthCal на русский.
	➜участников <b><a href='http://ubuntu.gr'>Ubuntu-gr community</a></b> за выявление ошибок,
советы по функционалу и общую поддержку.</span>
 <span size="small">Это приложение было написано для <a href="http://developer.ubuntu.com/showdown/">Ubuntu App Showdown contest</a> в
июле 2012, в качестве практики.

Код индикатора написан на <a href="http://www.python.org/">Python</a>/<a href="http://www.pygtk.org/">PyGTK</a>, в то время, как основная 
часть написана на <a href="http://www.perl.org/">Perl</a>/<a href="http://search.cpan.org/~xaoc/Gtk2-1.244/lib/Gtk2.pm">GTK2</a>.</span>
 Об OrthCal О Perl О gtk2-perl Неделя 2-я по Пятидесятнице, Всех святых в земле Российской просиявших Вознесение Господне Неделя сыропустная (Прощёное воскресенье). Воспоминание Адамова изгнания. Завершен Использованные источники Источник месяцеслова День Святого Духа Неделя 7-я по Пасхе, святых отцев I Вселенского Собора Поиск по месяцеслову -- OrthCal Начало Великого поста Великая Пятница Великий Понедельник Великая Суббота Великий Четверг Великий Вторник Великая Среда Язык Отдание праздника Пасхи Лицензия Неделя мясопустная, о Страшнем суде Преполовение Пятидесятницы Праздников не найдено. Настройки OrthCal Вербное воскресенье Пасха Пасхалия -- OrthCal Рассчитать дату Пасхи День Святой Троицы. Пятидесятница. Неделя о мытаре и фарисее Поиск праздников по названию Запускать OrthCal при старте системы. Неделя 1-я по Пятидесятнице, Всех святых Неделя 3-я по Пасхе, свв. жен-мироносиц Неделя 1-я Великого поста. Торжество Православия Неделя 2-я Великого поста. Свт. <b>Григория</b> Паламы, архиеп. Солунского Неделя 4-я Великого поста. Прп. <b>Иоанна</b> Лествичника Неделя 5-я Великого поста. Прп. <b>Марии</b> Египетской Неделя 2-я по Пасхе (Антипасха). Воспоминание уверения апостола <b>Фомы</b> Неделя 6-я по Пасхе, о слепом Неделя 3-я Великого поста, Крестопоклонная Неделя 4-я по Пасхе, о расслабленом Неделя о блудном сыне Неделя 5-я по Пасхе, о самаряныне Благодарности Празднование Пасхи в  Перевести OrthCal... _Правка _Файл _Справка _Инструменты 